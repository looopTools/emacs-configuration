(setq exec-path (append exec-path '("/usr/local/bin"))) ;; Path to brew executables
(exec-path-from-shell-initialize)
;; Use scadi os x keyboard
(setq mac-option-key-is-meta nil)
(setq mac-command-key-is-meta t)
(setq mac-command-modifier 'meta)
(setq mac-option-modifier nil)

(provide 'mac-os-settings)
