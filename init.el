(setq package-archives '(("melpa" . "http://melpa.org/packages/")
                         ("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

(setq package-install-upgrade-built-in t)

(require 'use-package)
(use-package auctex
  :defer t
  :ensure t)
(use-package company
  :ensure t)
(use-package cmake-mode
  :ensure t)
(use-package exec-path-from-shell
  :ensure t)
(use-package flycheck
  :ensure t)
(use-package flyspell
  :ensure t)
(use-package lsp-mode
  :ensure t)
(use-package markdown-mode
  :ensure t)
(use-package magit
  :ensure t)
(use-package move-text
  :ensure t)
(use-package powerline
  :ensure t)
(use-package scss-mode
  :ensure t)
(use-package web-beautify
  :ensure t)
(use-package web-mode
  :ensure t)
(use-package rust-mode
  :ensure t)
(use-package writeroom-mode
  :ensure t)
(use-package monokai-theme
  :ensure t)
<<<<<<< HEAD
(use-package clang-format
=======
(use-package protobuf-mode
  :ensure t)
(use-package ggtags
  :ensure t)
(use-package yaml-mode
>>>>>>> refs/remotes/origin/master
  :ensure t)

(scroll-bar-mode -1) ;; remove scrollbar
(show-paren-mode 1) ;; show matching parenthesis
(tool-bar-mode -1) ;; Remove the toolbar in the top
(menu-bar-mode -1) ;; remove menue bar
(setq ring-bell-function 'ignore) ;; Removes the warning bell
;;; Customise some things that make emacs easier to use
(setq completion-cycle-threshold t) ;; Allows for auto complete to cycle through options 
(setq default-directory "~/") ;; Set the default place to look for files
(setq delete-old-versions t) ;; Remove old versions of files on save 
(setq inhibit-startup-message t) ;; Remove the start-up message from emacs
(load-theme 'monokai t)

(when (string-equal system-type "darwin")
   (setq mac-option-key-is-meta nil)
   (setq mac-command-key-is-meta t)
   (setq mac-command-modifier 'meta)
   (setq mac-option-modifier nil)
   (setq ispell-program-name "/usr/local/bin/aspell")
   (setq exec-path (append exec-path '("/usr/local/bin"))) ;; Path to brew executables
   (exec-path-from-shell-initialize)
)

(require 'move-text)
(move-text-default-bindings)

(require 'ox-latex)

(global-set-key (kbd "C-c c") 'org-capture)

(when (string-equal system-type "darwin")
  (setq org-capture-templates
    '(("t" "Todo" entry (file+headline "~/Documents/AU/work-todos/todos.org" "Tasks")
       "* TODO %?\n  %i\n  %a")
      ("a" "Todo" entry (file+headline "~/Documents/author/author-notes/todos.org" "Tasks")
       "* TODO %?\n  %i\n  %a")
      ("j" "Journal" entry (file+datetree "~/Documents/AU/lab-journal-au/journal.org")
       "* %?\nEntered on %U\n  %i\n  %a")
      ("e" "Journal" entry (file+datetree "~/Documents/author/author-notes/journal.org")
       "* %?\nEntered on %U\n  %i\n  %a"))))

;;; Exports
;;; Add beamer
(add-to-list 'org-latex-classes
             '("beamer"
               "\\documentclass\[presentation\]\{beamer\}"
               ("\\section\{%s\}" . "\\section*\{%s\}")
               ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
               ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

(add-to-list 'org-latex-packages-alist '("" "listings"))
(add-to-list 'org-latex-packages-alist '("" "tikz" t))
(setq org-latex-listings t)
(setq org-image-actual-width nil)

(eval-after-load "preview"
  '(add-to-list 'preview-default-preamble
   "\\PreviewEnvironment{tikzpicture}" t))  

(setq org-ref-default-type "cref")

(setq org-latex-pdf-process
      '("pdflatex -interaction nonstopmode -output-directory %o %f"
        "bibtex %b"
        "pdflatex -interaction nonstopmode -output-directory %o %f"
        "pdflatex -interaction nonstopmode -output-directory %o %f"))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (latex . t)))

;;; Adding latex class for ACM

(add-to-list 'org-latex-classes
             '("ACMART"
               "\\documentclass{acmart}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(require 'powerline)
(powerline-default-theme)

(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)

(require 'ispell)

(setq ispell-program-name "aspell")
(setq ispell-dictionary "en_GB")

(add-hook 'c++-mode 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode) 
  (add-hook 'org-mode 'flyspell-mode) 
  (add-hook 'python-mode 'flyspell-mode)
  (add-hook 'markdown-mode-hook 'flyspell-mode)
  (add-hook 'org-mode-hook 'flyspell-mode)
(setq org-image-actual-width (list 550))

;;(require 'auctex)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook
      (lambda()
        (local-set-key [C-tab] 'TeX-complete-symbol)))
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)
(setq reftex-default-bibliography '("/Users/larsnielsen/Dev/bibtex-references")) ;; Testing for MacOS

(global-set-key (kbd "C-x g s") 'magit-status)

(setq c-default-style "stroustrup")
(setq-default indent-tabs-mode nil) ;; Disabling tabs (need for AU coding style)
(require 'cc-mode)
(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)
(c-set-offset 'template-args-cont 4 nil)
;;; Proper indent in hpp files inline code:
(c-set-offset 'inline-open 0)
;;; Setup C++ standard
(add-hook 'c++-mode-hook (lambda () (setq flycheck-clang-language-standard "c++17")))

(load "/opt/homebrew/opt/llvm/share/emacs/site-lisp/llvm/clang-format.el")
(global-set-key [C-M-tab] 'clang-format-region)

(require 'writeroom-mode)
(setq writeroom-width 140)

(add-hook 'c-mode-hook 'lsp)
(add-hook 'c++-mode-hook 'lsp)
(add-hook 'ruby-mode 'lsp)
(add-hook 'python-mode 'lsp)

(add-hook 'after-init-hook 'global-company-mode)

(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))
