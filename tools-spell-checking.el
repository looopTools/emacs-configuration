;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Spell checking                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)
(require 'ispell)

;;;(setq ispell-program-name "hunspell")
(setq ispell-program-name "aspell")
(setq ispell-dictionary "en_GB")

;;; Add spelling ot modes 
(add-hook 'c++-mode 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode) 
(add-hook 'org-mode 'flyspell-mode) 
(add-hook 'python-mode 'flyspell-mode)
(add-hook 'markdown-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'flyspell-mode)

(provide 'tools-spell-checking)
