;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; org settings                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org)
(require 'ox-latex)
(require 'org-ref)

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

;;; Logging
(setq org-todo-keywords
      '((sequence "TODO" "DOING" "FEEDBACK" "VERIFY" "|" "DONE" "DELEGATED")))

(setq org-log-done t)

;; (setq org-capture-templates
;;       '(("t" "Todo" entry (file+headline "~/org/gtd.org" "Tasks")
;;          "* TODO %?\n  %i\n  %a")
;;         ("j" "Journal" entry (file+datetree "~/org/journal.org")
;;          "* %?\nEntered on %U\n  %i\n  %a")))
(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/Documents/AU/work-todos/todos.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("a" "Todo" entry (file+headline "~/Documents/author/author-notes/todos.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("j" "Journal" entry (file+datetree "~/Documents/AU/lab-journal-au/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")
        ("e" "Journal" entry (file+datetree "~/Documents/author/author-notes/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")))

;;; Exports
;;; Add beamer
(add-to-list 'org-latex-classes
             '("beamer"
               "\\documentclass\[presentation\]\{beamer\}"
               ("\\section\{%s\}" . "\\section*\{%s\}")
               ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
               ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

(add-to-list 'org-latex-packages-alist '("" "listings"))
(add-to-list 'org-latex-packages-alist '("" "tikz"))
(setq org-latex-listings t)
(setq org-image-actual-width nil)


(setq org-ref-default-type "cref")

(setq org-latex-pdf-process
      '("pdflatex -interaction nonstopmode -output-directory %o %f"
	"bibtex %b"
	"pdflatex -interaction nonstopmode -output-directory %o %f"
	"pdflatex -interaction nonstopmode -output-directory %o %f"))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (latex . t)))

;;; Adding latex class for ACM

(add-to-list 'org-latex-classes
             '("ACMART"
               "\\documentclass{acmart}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))



(provide 'tools-org-settings)
